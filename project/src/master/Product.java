/**
 * 
 */
package master;

import java.text.NumberFormat;


/**
 * The product class that simply holds the value and the price.
 * 
 * @author mbilgic
 *
 */
public class Product {
	private double value;
	private double price;

	protected Product(double v)
	{
		this(v, 0);
	}
	
	protected Product(double v, double pr)
	{
		this.value = v;
		this.price = pr;
	}

	/**
	 * @return the value
	 */
	public double getValue() {
		return value;
	}

	/**
	 * @return the price
	 */
	public double getPrice() {
		return price;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		NumberFormat nf = NumberFormat.getCurrencyInstance();
		return "Product [value=" + nf.format(value) + ", price=" + nf.format(price) + "]";
	}
}
