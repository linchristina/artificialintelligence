/**
 * 
 */
package agents;

import java.util.ArrayList;
import java.util.Random;

import master.Product;

/**
 * This agent flips a coin to decide whether to
 * 	buy a product, and
 * 	increase its bid.
 * 
 * It ignores any other information, whether it be
 * 	the price, the value, the features, or probabilities.	
 * 
 * @author mbilgic
 *
 */
public class FlipCoinAgent extends Agent {
	
	private Random rand;

	public FlipCoinAgent(String id, int seed) {
		super(id);
		this.rand = new Random(seed);
	}

	/* (non-Javadoc)
	 * @see agents.Agent#willBuy(master.Product, double)
	 */
	@Override
	public boolean willBuy(Product prod, double probOfGood) {
		
		//Flip a coin
		double fc = rand.nextDouble();
		
		if(fc > 0.5)
			return true;
		else
			return false;
	}

	/* 
	 * There is no point in learning.
	 * 
	 * (non-Javadoc)
	 * @see agents.Agent#learn(java.util.ArrayList)
	 */
	@Override
	public void learn(ArrayList<ArrayList<String>> trainingInstances) {
		// TODO Auto-generated method stub
		
	}

	/* 
	 * This probability will be ignored.
	 * 
	 * (non-Javadoc)
	 * @see agents.Agent#computeProbOfGood(java.util.ArrayList)
	 */
	@Override
	public double computeProbOfGood(ArrayList<String> prodFeatures) {
		// TODO Auto-generated method stub
		return 0;
	}



}
