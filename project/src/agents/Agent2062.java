package agents;

import java.util.ArrayList;
import java.util.Random;

import master.Product;

public class Agent2062 extends Agent {
	
	private Random rand;
	
	public Agent2062(String id) {
		super(id);
	}
	
	public boolean willBuy(Product prod, double probOfGood) {
		if (probOfGood < 0.5 && (prod.getPrice() <= prod.getValue()*0.15))
			return true;
		else if (probOfGood > 0.5 && (prod.getPrice() >= prod.getValue()*0.8))
			return false;
		else if (probOfGood > 0.5)
			return true;
		else
			return false;
	}
	
	public void learn(ArrayList<ArrayList<String>> trainingInstances) {
		
	}
	
	public double computeProbOfGood(ArrayList<String> prodFeatures) {
		return 0;
	}

}
