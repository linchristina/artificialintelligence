package master;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

import agents.*;


/**
 * Constructs products and sells them.
 * 
 * @author mbilgic
 *
 */
public class LearningAgentSimulator {


	//The cap on the maximum value of the product
	private final static double MAXIMUMVALUE = 100000;

	//The initial money the agent has
	private final static double INITIALMONEY = 1000;
	//The amount the agent earns daily from other sources
	private final static double DAILYEARNINGS = 100;
	

	//To see the full trace, set it true
	static boolean debug = false;


	/**
	 * @param args
	 */
	public static void main(String[] args) {

		//TODO Change this to the last four digits of your A#.
		int lastfourdigits = 2062;

		int[] seeds = {0, 1, 2, 3, lastfourdigits};
		
		if(args.length<1)
		{
			System.err.println("Usage: java LearningAgentSimulator <productFile>");
			System.exit(1);
		}
		
		String instancesFile = args[0];
		
		ArrayList<ArrayList<String>> allInstances = LearningAgentSimulator.readInstances(instancesFile);
		
		Collections.shuffle(allInstances, new Random(0));
		
		int foldSize = allInstances.size()/seeds.length;			
	
		double[] average = new double[foldSize];

		for(int s=0; s<seeds.length; s++)
		{
			// [foldSize*s, foldSize*(s+1)) are test, remaining are train
			
			ArrayList<ArrayList<String>> testInstances = new ArrayList<ArrayList<String>>();
			ArrayList<ArrayList<String>> trainInstances = new ArrayList<ArrayList<String>>();
			
			for(int i=0; i<allInstances.size(); i++)
			{
				if (i>= foldSize*s && i<foldSize*(s+1))
					testInstances.add(allInstances.get(i));
				else
					trainInstances.add(allInstances.get(i));
			}
			
			
			Agent agent = null;
			
			//TODO Change this to the agent you are simulating
						
			//RationalBaseline Agent
			//agent = new RationalBaselineAgent("RB");
	
			//AgentPhase21234. Your agent should have this type of constructor only.
			//agent = new AgentPhase22062("Agent2062");
			
			//AgentPhase3NNNN
			agent = new AgentPhase32062("Agent2062");
			
			LearningAgentSimulator.print("Simulating Agent"+agent);
			
			double[] dailyBalance = LearningAgentSimulator.simulateAnAgent(agent, trainInstances, testInstances, seeds[s]);
			
			for(int d=0; d<dailyBalance.length;d++)
			{
				average[d] += dailyBalance[d]/(seeds.length);
			}
		}
		

		NumberFormat nf = NumberFormat.getCurrencyInstance();
		
		System.out.println("Day\tAverage Balance");
		
		for(int d=0; d < average.length ; d++)
		{
			System.out.println((d+1)+"\t"+nf.format(average[d]));
		}

	}


	/**
	 * You should NOT modify this method, except to print less/more info.
	 * @param agent
	 * @param trainInstances
	 * @param testInstances
	 * @param seed
	 * @return
	 */
	private static double[] simulateAnAgent(Agent agent, ArrayList<ArrayList<String>> trainInstances, ArrayList<ArrayList<String>> testInstances, int seed)
	{

		Bank bank = new Bank();

		agent.setBank(bank);

		//deposit the initial money
		bank.deposit(agent, LearningAgentSimulator.INITIALMONEY);

		Random rand = new Random(seed);

		double[] dailyBalance = new double[testInstances.size()];

		NumberFormat nf = NumberFormat.getCurrencyInstance();

		LearningAgentSimulator.print("\nSeed="+seed);
		
		LearningAgentSimulator.print("Agent is learning the concept.");
		
		agent.learn(trainInstances);
		
		LearningAgentSimulator.print("Agent completed the learning process.");
		
		
		for(int p=0;p<testInstances.size();p++)
		{
			
			
			LearningAgentSimulator.print("Product #: "+(p+1));

			LearningAgentSimulator.print("The balance at the beginning of the day is: "+ nf.format(bank.balance(agent)));

			//The maximum value of the product
			double maxValue = Math.min(bank.balance(agent), LearningAgentSimulator.MAXIMUMVALUE);

			//sample a value for the product
			double value = rand.nextDouble()*maxValue;

			//sample a price for the product
			double price = rand.nextDouble()*value;
			
			Product prod = new Product(value, price);
			
			LearningAgentSimulator.print("Product is "+prod);
			
			ArrayList<String> prodFeatures = testInstances.get(p);
			
			String workingCondition = prodFeatures.get(prodFeatures.size()-1);
			
			boolean productWorking = workingCondition.equalsIgnoreCase("G");
			
			//Remove the class
			prodFeatures.remove(prodFeatures.size()-1);			
			
			boolean willBuy = agent.willBuy(prod, prodFeatures);
			
			prodFeatures.add(workingCondition);

			if(willBuy)
			{

				LearningAgentSimulator.print("Agent "+agent+" decides to buy it.");

				//withdraw the product's price from the agent's account
				bank.withdraw(agent, prod.getPrice());

				if(productWorking) // the product is in working condition
				{
					LearningAgentSimulator.print("Good call: the product is in working condition.");
					LearningAgentSimulator.print("The agent's profit is "+ nf.format((prod.getValue()-prod.getPrice())) +".");
					//deposit the product's value to the agent's account
					bank.deposit(agent, prod.getValue());
				}
				else
				{
					LearningAgentSimulator.print("Bad call: the product is faulty.");
					LearningAgentSimulator.print("The agent loses "+nf.format(prod.getPrice()) +".");
					//Do not deposit anything
				}
			}
			else
			{
				LearningAgentSimulator.print("Agent "+agent+" decides not to buy it.");
				if(productWorking)
				{
					LearningAgentSimulator.print("Missed opportunity: the product was in working condition.");
				}
				else
				{
					LearningAgentSimulator.print("Good call: the product was faulty.");
				}
			}

			bank.deposit(agent, LearningAgentSimulator.DAILYEARNINGS);

			dailyBalance[p] = bank.balance(agent);

			LearningAgentSimulator.print((p+1) + "\t" + nf.format(dailyBalance[p]));
		}
		
		return dailyBalance;

	}
	
	private static void print(String text)
	{
		if(LearningAgentSimulator.debug)
			System.out.println(text);
	}

	/**
	 * Read instances from a csv file, where the first line is header information.
	 * @param file
	 * @return
	 */
	private static ArrayList<ArrayList<String>> readInstances(String file) {
		ArrayList<ArrayList<String>> instances = new ArrayList<ArrayList<String>>();
		try {
			BufferedReader br = new BufferedReader(new FileReader(file));
			String line = br.readLine();//Header
			
			String[] fields = line.split(",");
			
			int numFields = fields.length; // number of attributes + class
			
			while((line = br.readLine()) != null)
			{
				ArrayList<String> instance = new ArrayList<String>(numFields);
				
				fields = line.split(",");
				
				for(int i=0;i<numFields;i++)
				{
					instance.add(fields[i]);
				}
				
				instances.add(instance);
				
			}
			br.close();
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return instances;
	}

}