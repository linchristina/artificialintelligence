import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

import aima.core.environment.eightpuzzle.EightPuzzleBoard;
import aima.core.environment.eightpuzzle.EightPuzzleFunctionFactory;
import aima.core.environment.eightpuzzle.EightPuzzleGoalTest;
import aima.core.environment.eightpuzzle.ManhattanHeuristicFunction;
import aima.core.environment.eightpuzzle.MisplacedTilleHeuristicFunction;
import aima.core.probability.bayes.Node;
import aima.core.search.framework.Metrics;
import aima.core.search.framework.NodeExpander;
import aima.core.search.framework.Problem;
import aima.core.search.framework.Search;
import aima.core.search.framework.SearchAgent;
import aima.core.search.framework.TreeSearch;
import aima.core.search.informed.AStarSearch;
import aima.core.search.uninformed.IterativeDeepeningSearch;

/**
 * 
 */


public class EightPuzzle {

	/**
	 * @param args
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception {
		
		for(int depth=1; depth<25; depth++)
		{
			
			double avgNo = 0;
			int totNo = 0;
			int branch = 0;
			
			System.out.println("Depth:\t"+depth);
			
			
			List<EightPuzzleBoard> boards = EightPuzzle.randomBoards(depth, 10, depth);
			
			
			long begin = System.currentTimeMillis();
			
			for(EightPuzzleBoard board: boards)
			{
				Problem problem = new Problem(board,
						EightPuzzleFunctionFactory.getActionsFunction(),
						EightPuzzleFunctionFactory.getResultFunction(),
						new EightPuzzleGoalTest());
				
				
				//Choose one of them
				//Search search = new IterativeDeepeningSearch();			
				//Search search = new AStarSearch(new TreeSearch(), new MisplacedTilleHeuristicFunction());
				//Search search = new AStarSearch(new TreeSearch(), new ManhattanHeuristicFunction());
				Search search = new AStarSearch(new TreeSearch(), new IterDeepHeuristicFunction());
				
				@SuppressWarnings("unused")
				SearchAgent agent = new SearchAgent(problem, search);
				Metrics metrics = search.getMetrics();
				
				System.out.println(metrics);
				
				totNo += metrics.getInt(NodeExpander.METRIC_NODES_EXPANDED);
				branch++;
				
			}
			
			long end = System.currentTimeMillis();
			avgNo = (1.0 * totNo) / branch;
			
			System.out.println("Took:\t"+ (end-begin)/1000.0 + " seconds.");
			
			System.out.println("Total Nodes:\t" + totNo);
			System.out.println("Branches:\t" + branch);
			System.out.println("Average Nodes:\t" + avgNo + "\n");
			
		}
		

	}
	
	/**
	 * Return at max "numBoards" of random boards with a solution depth of "depth", using random number seed "seed".
	 * @param depth
	 * @param numBoards
	 * @param seed
	 * @return
	 */
	public static List<EightPuzzleBoard> randomBoards(int depth, int numBoards, int seed)
	{		
		
		int currentLevelDepth = 0;
		
		Set<EightPuzzleBoard> previousLevel = new HashSet<EightPuzzleBoard>();
		
		List<EightPuzzleBoard> currentLevel = new ArrayList<EightPuzzleBoard>();
		
		List<EightPuzzleBoard> nextLevel = new ArrayList<EightPuzzleBoard>();
		
		EightPuzzleBoard initialBoard = new EightPuzzleBoard(new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8 });
		
		currentLevel.add(initialBoard);
		
		while(currentLevelDepth < depth)
		{
			for(int c=0; c<currentLevel.size(); c++)
			{
				EightPuzzleBoard board = currentLevel.get(c);
				
				
				
				if(board.canMoveGap(EightPuzzleBoard.LEFT))
				{
					EightPuzzleBoard nextBoard = new EightPuzzleBoard(board);
					
					nextBoard.moveGapLeft();
					
					if(!previousLevel.contains(nextBoard))
					{
						nextLevel.add(nextBoard);
					}
				}
				
				if(board.canMoveGap(EightPuzzleBoard.RIGHT))
				{
					EightPuzzleBoard nextBoard = new EightPuzzleBoard(board);
					
					nextBoard.moveGapRight();
					
					if(!previousLevel.contains(nextBoard))
					{
						nextLevel.add(nextBoard);
					}
				}
				
				if(board.canMoveGap(EightPuzzleBoard.UP))
				{
					EightPuzzleBoard nextBoard = new EightPuzzleBoard(board);
					
					nextBoard.moveGapUp();
					
					if(!previousLevel.contains(nextBoard))
					{
						nextLevel.add(nextBoard);
					}
				}
				
				if(board.canMoveGap(EightPuzzleBoard.DOWN))
				{
					EightPuzzleBoard nextBoard = new EightPuzzleBoard(board);
					
					nextBoard.moveGapDown();
					
					if(!previousLevel.contains(nextBoard))
					{
						nextLevel.add(nextBoard);
					}
				}				
				
			}
			
			previousLevel.clear();
			previousLevel.addAll(currentLevel);
			currentLevel.clear();
			currentLevel.addAll(nextLevel);
			nextLevel.clear();
			currentLevelDepth++;
			
		}
		
		if(currentLevel.size() > numBoards)
		{
			Collections.shuffle(currentLevel, new Random(seed));
			
			List<EightPuzzleBoard> newC = new ArrayList<EightPuzzleBoard>();
			
			for(int c=0; c<numBoards; c++)
			{
				newC.add(currentLevel.get(c));
			}
			
			currentLevel = newC;
		}
		
		
		return currentLevel;
	}

}
