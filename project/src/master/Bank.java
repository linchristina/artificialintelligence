package master;
import java.util.HashMap;
import java.util.Map;

import agents.Agent;


/**
 * Keeps track of the money the agents have.
 * 
 * @author mbilgic
 *
 */
public class Bank {
	
	private Map<Agent, Double> money;
	
	public Bank()
	{
		this.money = new HashMap<Agent, Double>();
	}
	
	/**
	 * Return the balance for a given agent.
	 * 
	 * @param a
	 * @return
	 */
	public double balance(Agent a)
	{
		
		if(money.containsKey(a))
			return money.get(a);
		else
			return 0;
	}
	
	/**
	 * Deposit a given amount to the agent a's account.
	 * @param a
	 * @param amount
	 */
	protected void deposit(Agent a, double amount)
	{
		double balance = this.balance(a);
		
		balance += amount;
		
		this.money.put(a, balance);
	}
	
	/**
	 * Withdraw the amount from the agent a's account.
	 * @param a
	 * @param amount
	 */
	protected void withdraw(Agent a, double amount)
	{
		amount = -amount;
		this.deposit(a, amount);
	}
	
}
