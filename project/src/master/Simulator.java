package master;

import java.text.NumberFormat;
import java.util.Random;

import org.apache.commons.math3.distribution.BetaDistribution;

import agents.*;


/**
 * Constructs products and sells them.
 * 
 * @author mbilgic
 *
 */
public class Simulator {

	//Number of days
	private final static int NUMDAYS = 1000;
	//The cap on the maximum value of the product
	private final static double MAXIMUMVALUE = 50000;

	//The initial money the agent has
	private final static double INITIALMONEY = 1000;
	//The amount the agent earns daily from other sources
	private final static double DAILYEARNINGS = 100;
	
	//The markets with the ratio of good v.s. bad products
	private final static int[] UNFAVORABLE = {1, 3};
	private final static int[] FAIR = {1, 1};
	private final static int[] FAVORABLE = {3, 1};

	//To see the full trace, set it true
	static boolean debug = false;


	/**
	 * @param args
	 */
	public static void main(String[] args) {

		//TODO Change this to the last four digits of your A#.
		int lastfourdigits = 2062;

		int[] seeds = {0, 1, 2, 3, 4, 5, 6, 7, 8, lastfourdigits};
		
		//TODO Change this to the market condition you are simulating
		int[] market_odds = Simulator.FAIR;

		double[] average = new double[Simulator.NUMDAYS];

		for(int seed: seeds)
		{
			
			Agent agent = null;
			
			//TODO Change this to the agent you are simulating
			
			//Flip coin
			//agent = new FlipCoinAgent("FC", seed);
			
			//Half probability agent
			//agent = new HalfProbAgent("HP");
			
			//Percent Believer Agent(0)
			//agent = new PercentBeliever("PB0", 0);
			
			//Percent Believer Agent(25)
			//agent = new PercentBeliever("PB25", 25);
			
			//Percent Believer Agent(50)
			//agent = new PercentBeliever("PB50", 50);
			
			//Percent Believer Agent(75)
			//agent = new PercentBeliever("PB75", 75);
			
			//Percent Believer Agent(100)
			//agent = new PercentBeliever("PB100", 100);
			
			//Agent1234. Your agent should have this type of constructor only.
			agent = new Agent2062("2062");
			
			Simulator.print("Simulating Agent"+agent);
			
			double[] dailyBalance = Simulator.noLearningCase(agent, market_odds, seed);
			
			for(int d=0; d<Simulator.NUMDAYS;d++)
			{
				average[d] += dailyBalance[d]/(seeds.length);
			}
		}
		

		NumberFormat nf = NumberFormat.getCurrencyInstance();
		
		System.out.println("Day\tAverage Balance");
		
		for(int d=0; d < Simulator.NUMDAYS ; d++)
		{
			System.out.println((d+1)+"\t"+nf.format(average[d]));
		}

	}

	/**
	 * Given an agent and a seed, simulates agent. 
	 * You should NOT modify this method, except to print less/more info.
	 * @param agent
	 * @param seed
	 * @return The daily balance of the agent
	 */
	public static double[] noLearningCase(Agent agent, int[] market_odds, int seed)
	{

		Bank bank = new Bank();

		agent.setBank(bank);

		//deposit the initial money
		bank.deposit(agent, Simulator.INITIALMONEY);

		Random rand = new Random(seed);

		double[] dailyBalance = new double[Simulator.NUMDAYS];

		NumberFormat nf = NumberFormat.getCurrencyInstance();

		Simulator.print("\nSeed="+seed);
		
		BetaDistribution bd = new BetaDistribution(market_odds[0], market_odds[1]);
		
		bd.reseedRandomGenerator(seed);
		
		for(int d=0;d<Simulator.NUMDAYS;d++)
		{
			Simulator.print("Day "+(d+1));

			Simulator.print("The balance at the beginning of the day is: "+ nf.format(bank.balance(agent)));

			//The maximum value of the product
			double maxValue = Math.min(bank.balance(agent), Simulator.MAXIMUMVALUE);

			//sample a value for the product
			double value = rand.nextDouble()*maxValue;

			//sample a price for the product
			double price = rand.nextDouble()*value;

			//sample a prob for being in the good condition
			double prob = bd.sample();

			//Is the product in working condition
			double draw = rand.nextDouble();

			boolean productWorking = (draw <= prob);

			Product prod = new Product(value, price);

			Simulator.print("Product is "+prod);

			Simulator.print("The probability of product being in working condition is "+prob);

			boolean willBuy = agent.willBuy(prod, prob);

			if(willBuy)
			{

				Simulator.print("Agent "+agent+" decides to buy it.");

				//withdraw the product's price from the agent's account
				bank.withdraw(agent, prod.getPrice());

				if(productWorking) // the product is in working condition
				{
					Simulator.print("Good call: the product is in working condition.");
					Simulator.print("The agent's profit is "+ nf.format((prod.getValue()-prod.getPrice())) +".");
					//deposit the product's value to the agent's account
					bank.deposit(agent, prod.getValue());
				}
				else
				{
					Simulator.print("Bad call: the product is faulty.");
					Simulator.print("The agent loses "+nf.format(prod.getPrice()) +".");
					//Do not deposit anything
				}
			}
			else
			{
				Simulator.print("Agent "+agent+" decides not to buy it.");
				if(productWorking)
				{
					Simulator.print("Missed opportunity: the product was in working condition.");
				}
				else
				{
					Simulator.print("Good call: the product was faulty.");
				}
			}

			bank.deposit(agent, Simulator.DAILYEARNINGS);

			dailyBalance[d] = bank.balance(agent);

			Simulator.print((d+1) + "\t" + nf.format(dailyBalance[d]));
		}
		
		return dailyBalance;

	}
	
	private static void print(String text)
	{
		if(Simulator.debug)
			System.out.println(text);
	}

}
