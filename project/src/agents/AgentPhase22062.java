package agents;

import java.util.ArrayList;

public class AgentPhase22062 extends RationalBaselineAgent {
	
	private double[] features = new double[15];
	private double featprob = 0;
	
	public AgentPhase22062(String id) {
		super(id);
	}
	
	public void learn(ArrayList<ArrayList<String>> trainingInstances) {
		
		int numGoodProducts = 0;
		int numFeats = 0, i = 0;
		
		for(ArrayList<String> product: trainingInstances)
		{
			String condition = product.get(product.size()-1);
			numFeats = product.size()-1;
			
			if (condition.equalsIgnoreCase("G"))
				numGoodProducts++;
			
			for (i = 0; i < numFeats; i++) {
				if (product.get(i).equalsIgnoreCase("T") && condition.equalsIgnoreCase("G"))
					features[i]++;
			}
			for (i = 0; i < numFeats; i++) {
				features[i] = features[i]/trainingInstances.size();
				featprob += features[i];
			}
		}
		this.marketCondition = numGoodProducts*1.0/trainingInstances.size();

	}
	
	public double computeProbOfGood(ArrayList<String> prodFeatures) {
		return this.marketCondition * featprob;
	}

}
