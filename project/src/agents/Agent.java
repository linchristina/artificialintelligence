package agents;
import java.util.ArrayList;

import master.Bank;
import master.Product;

/**
 * The base Agent class. All agents should inherit this class.
 * 
 * In the single agent case, this agent receives a product and decides to buy the product or not; a one time decision for a given product.
 * 
 * In the multi-agent case, it receives the product with the newDay method, receives maxBidder's id and bid through currentBidInfo, and when asked, it provides a bid through myBidIs function. It learns the result of the bidding through endOfDay function.
 *
 * 
 * @author mbilgic
 *
 */
public abstract class Agent {
	
	private String id;
	private Bank bank;
	
	public Agent(String id)
	{
		this.id = id;
	}
	
	////////////////////////
	// DECISION MAKING ///
	////////////////////////

	/**
	 * Given a product prod and the probability of it being in a good condition,
	 * the agent decides whether to buy the product.
	 * 
	 * @param prod - The Product
	 * @param probOfGood - Probability of the product being in a good condition.
	 * @return True if the agent decides to buy the product.
	 */
	public abstract boolean willBuy(Product prod, double probOfGood);
	
	/**
	 * It serves the same functionality with {@link #willBuy(Product, double)}.
	 * In this method, instead of a probability, the agent is provided with a description of the product.
	 * This implementation just computes the probability of being in good condition using {@link #computeProbOfGood(ArrayList)},
	 * and then calls {@link #willBuy(Product, double)}.
	 * 
	 * @param prod - The Product
	 * @param prodFeatures - Features describing the product.
	 * @return True if the agent decides to buy the product.
	 */
	public final boolean willBuy(Product prod, ArrayList<String> prodFeatures) {
		double prob = computeProbOfGood(prodFeatures);
		return this.willBuy(prod, prob);
	}
	
	//////////////////////////
	// LEARN AND PREDICT /////
	//////////////////////////

	/**
	 * Learn a function that maps features into whether the product is Good or Bad.
	 * 
	 * @param trainingInstances - Each instance is represented by ArrayList<String>, where
	 * each entry represents a categorical feature value. The last feature represents the class (G/B).
	 */
	public /* abstract */ void learn(ArrayList<ArrayList<String>> trainingInstances) { }

	/**
	 * Given product features, predict whether the product's class is G.
	 * 
	 * @param prodFeatures - Features describing the product. These features do not include the class information.
	 * @return Probability of product being in a good condition.
	 */
	public /* abstract */ double computeProbOfGood(ArrayList<String> prodFeatures) { return 0; }
	
	
	
	//////////////////
	// GENERAL INFO //
	//////////////////
	
	/**
	 * 
	 * @return My current balance.
	 */
	public double myBalance()
	{
		return this.bank.balance(this);
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return this.id;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Agent [id=" + this.id + "]";
	}
	

	/**
	 * @param bank the bank to set
	 */
	public final void setBank(Bank bank) {
		this.bank = bank;
	}
	
}
