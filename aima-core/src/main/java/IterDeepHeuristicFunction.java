/**
 * 
 */


import aima.core.environment.eightpuzzle.EightPuzzleBoard;
import aima.core.environment.eightpuzzle.EightPuzzleFunctionFactory;
import aima.core.environment.eightpuzzle.EightPuzzleGoalTest;
import aima.core.search.framework.HeuristicFunction;
import aima.core.search.framework.Metrics;
import aima.core.search.framework.Problem;
import aima.core.search.framework.Search;
import aima.core.search.framework.SearchAgent;
import aima.core.search.uninformed.IterativeDeepeningSearch;

public class IterDeepHeuristicFunction implements HeuristicFunction {

	/* (non-Javadoc)
	 * @see aima.core.search.framework.HeuristicFunction#h(java.lang.Object)
	 */
	@Override
	public double h(Object state) {
		
		double h = 0;
		
		EightPuzzleBoard board = (EightPuzzleBoard) state;
		//TODO Write the necessary code.
		Search search = new IterativeDeepeningSearch();
		Metrics metrics = search.getMetrics();
		
		h = metrics.getDouble(IterativeDeepeningSearch.PATH_COST);
		
		
		return h;
	}

}
