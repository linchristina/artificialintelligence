/**
 * 
 */
package agents;

import java.util.ArrayList;

import master.Product;

/**
 * This agent buys a product (or bids as much as it can) when it believes
 * the product is in good condition, regardless of the value and the price of the product.
 * It believes the product is in good condition when the given or computed
 * probability is greater than 0.5.
 * 
 * @author mbilgic
 *
 */
public class HalfProbAgent extends Agent {

	public HalfProbAgent(String id) {
		super(id);
	}

	/* 
	 * Buys the product if probability of being in good condition is > 0.5.
	 * It ignores the value and the price of the product.
	 * (non-Javadoc)
	 * @see agents.Agent#willBuy(master.Product, double)
	 */
	@Override
	public boolean willBuy(Product prod, double probOfGood) {
		if(probOfGood > 0.5)
			return true;
		else
			return false;
	}

	/* (non-Javadoc)
	 * @see agents.Agent#learn(java.util.ArrayList)
	 */
	@Override
	public void learn(ArrayList<ArrayList<String>> trainingInstances) {
		// TODO This method still needs to be implemented
		
	}

	/* (non-Javadoc)
	 * @see agents.Agent#computeProbOfGood(java.util.ArrayList)
	 */
	@Override
	public double computeProbOfGood(ArrayList<String> prodFeatures) {
		// TODO This method still needs to be implemented
		return 0;
	}



}
