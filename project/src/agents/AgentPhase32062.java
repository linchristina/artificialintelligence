package agents;

import java.util.ArrayList;

public class AgentPhase32062 extends RationalBaselineAgent {
	
	private double[] features = new double[15];
	private int numGoodProducts;
	
	public AgentPhase32062(String id) {
		super(id);
	}
	
	public void learn(ArrayList<ArrayList<String>> trainingInstances) {
		
		int numFeats = 0, i;
		int[] numTrueFeats = new int[15];
		
		for(ArrayList<String> product: trainingInstances) {
			
			String condition = product.get(product.size()-1);
			numFeats = product.size()-1;
			
			if (condition.equalsIgnoreCase("G"))
				this.numGoodProducts++;
			
			for (i = 0; i < numFeats; i++) {
				if (product.get(i).equalsIgnoreCase("T") && condition.equalsIgnoreCase("G"))
					numTrueFeats[i]++;
			}
			
		}
		
		for (i = 0; i < numFeats; i++) {
			this.features[i] = ( numTrueFeats[i] + 1 )*1.0 / ( numGoodProducts + 2 );
		}
		
		this.marketCondition = ( this.numGoodProducts + 1 )*1.0 / ( trainingInstances.size() + 2);
	}
	
	public double computeProbOfGood(ArrayList<String> prodFeatures) {
		
		double prob;
		double probgood = this.marketCondition;
		double probbad = 1;
		int i = 0;
		
		for (String s: prodFeatures) {
			if (s.equalsIgnoreCase("T")) {
				probgood *= features[i];
				probbad *= 1 - features[i];
			}
			else if (s.equalsIgnoreCase("F")) {
				probgood *= 1 - features[i];
				probbad *= features[i];
			}
			i++;
		}
		prob = probgood + probbad;
		probgood = probgood / prob;
		
		return probgood;
	}

}
