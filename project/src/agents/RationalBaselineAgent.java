/**
 * 
 */
package agents;

import java.util.ArrayList;

import master.Product;

/**
 * @author mbilgic
 *
 */
public class RationalBaselineAgent extends Agent {
	
	//This agent learns only the market condition
	double marketCondition = 0.5;

	/**
	 * @param id
	 */
	public RationalBaselineAgent(String id) {
		super(id);
	}

	/* (non-Javadoc)
	 * @see agents.Agent#willBuy(master.Product, double)
	 */
	@Override
	public final boolean willBuy(Product prod, double probOfGood) {
		
		return (probOfGood*prod.getValue() > prod.getPrice());
		
	}

	/* (non-Javadoc)
	 * @see agents.Agent#learn(java.util.ArrayList)
	 */
	@Override
	public void learn(ArrayList<ArrayList<String>> trainingInstances) {
		
		int numGoodProducts = 0;
		
		for(ArrayList<String> product: trainingInstances)
		{
			String condition = product.get(product.size()-1);
			
			if (condition.equalsIgnoreCase("G"))
				numGoodProducts++;
			
		}
		
		this.marketCondition = numGoodProducts*1.0/trainingInstances.size();

	}

	/* (non-Javadoc)
	 * @see agents.Agent#computeProbOfGood(java.util.ArrayList)
	 */
	@Override
	public double computeProbOfGood(ArrayList<String> prodFeatures) {
		// Ignore all the features; simply return the market condition
		return this.marketCondition;
	}

}
