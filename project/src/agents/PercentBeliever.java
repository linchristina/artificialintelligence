/**
 * 
 */
package agents;

import java.util.ArrayList;

import master.Product;

/**
 * An agent that always believes that a product is worth only a fixed percent of its value.
 * The percent is provided initially.
 * 
 * @author mbilgic
 *
 */
public class PercentBeliever extends Agent {
	
	//The percent worth of a product. A number between [0,100].
	private int percentWorth;

	public PercentBeliever(String id, int pw) {
		super(id);
		this.percentWorth = pw;
	}

	/* (non-Javadoc)
	 * @see agents.Agent#willBuy(master.Product, double)
	 */
	@Override
	public boolean willBuy(Product prod, double probOfGood) {
		
		if(prod.getPrice() <= (prod.getValue()*this.percentWorth)/100)
			return true;
		else
			return false;
	}

	@Override
	public double computeProbOfGood(ArrayList<String> prodFeatures) {
		return 0;
	}

	@Override
	public void learn(ArrayList<ArrayList<String>> trainingInstances) {
		
	}

}
